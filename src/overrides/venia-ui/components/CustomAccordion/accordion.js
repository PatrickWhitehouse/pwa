import React, {useState} from 'react';
import {bool, func, shape, string} from 'prop-types';
import defaultClasses from './accordion.css';
import {mergeClasses} from '@magento/venia-ui/lib/classify';




const Accordion = props => {

    const [shown, setShown] = useState(false);

    const classes = mergeClasses(defaultClasses, props.classes);


    return(
        <div className={classes.accordion}>
            <span className={classes.title} onClick={() => setShown(!shown)}>Title</span>
            <div className={classes.content} style={{display: shown ? 'block' : 'none'}}>Content</div>
        </div>
    );
}

Accordion.propTypes = {
    classes: shape({
        accordion: string,
        content: string,
        title: string
    })
}

export default Accordion;
